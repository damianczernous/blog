/*
 * (c) sanecoders.com
 */
package com.sanecoders.immutablelife;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.junit.Test;

/**
 * @author Damian Czernous
 */
public class GivenHashMap
{
    /**
     * For each value in the map, HashMap saves hashcode for fast searching. If the key object will change then
     * hashcodes won't match (saved & new one), because hashing algorithm is based on the key values. This is why the
     * key object should be immutable or mutable with unmodifiable hashcode (not value based hashing algorithm).
     */
    @Test
    public void whenKeyMutableThenStrangeThingsMayHappen()
    {
        final Map<MutableWeaponMapKey, Verdict> rpsJudgements = new HashMap<>();

        // create a key
        final MutableWeaponMapKey mutableKey = new MutableWeaponMapKey( Weapon.PAPER, Weapon.ROCK );

        // Paper wins with Rock
        rpsJudgements.put( mutableKey, Verdict.WIN );

        // change key's first weapon to Scissors
        mutableKey.setWeapon1( Weapon.SCISSORS );

        // now Scissors losses with Rock
        rpsJudgements.put( mutableKey, Verdict.LOSS );

        // both keys in the map:
        for( MutableWeaponMapKey key : rpsJudgements.keySet() )
        {
            // are equal
            assertTrue( new MutableWeaponMapKey( Weapon.SCISSORS, Weapon.ROCK ).equals( key ) );

            // have same hashcodes
            assertEquals( new MutableWeaponMapKey( Weapon.SCISSORS, Weapon.ROCK ).hashCode(), key.hashCode() );

            // and point on a same value in the map
            assertEquals( Verdict.LOSS, rpsJudgements.get( key ) );
        }

        // but we can't access Verdict.WIN (for Paper & Rock pair) by key
        assertEquals( null, rpsJudgements.get( new MutableWeaponMapKey( Weapon.PAPER, Weapon.ROCK ) ) );
        assertEquals( 2, rpsJudgements.size() );

        // however
        final Iterator<Verdict> verdicts = extract( rpsJudgements ).iterator();

        // we can access Verdict.WIN by iterating map values
        assertEquals( Verdict.WIN, verdicts.next() );
        assertEquals( Verdict.LOSS, verdicts.next() );
    }

    @Test
    public void whenImmutableKeyThenWeAreSafe()
    {
        final Map<WeaponMapKey, Verdict> toTest = new HashMap<>();

        // create a key
        final WeaponMapKey key = new WeaponMapKey( Weapon.PAPER, Weapon.ROCK );

        // Paper wins with Rock
        toTest.put( key, Verdict.WIN );

        // create new key (it's not possible to change previous one)
        final WeaponMapKey newKey = new WeaponMapKey( Weapon.SCISSORS, Weapon.ROCK );

        // now Scissors losses with Rock
        toTest.put( newKey, Verdict.LOSS );

        // we can access both entries using keys
        assertEquals( Verdict.WIN, toTest.get( new WeaponMapKey( Weapon.PAPER, Weapon.ROCK ) ) );
        assertEquals( Verdict.LOSS, toTest.get( new WeaponMapKey( Weapon.SCISSORS, Weapon.ROCK ) ) );
        assertEquals( 2, toTest.size() );
    }

    private List<Verdict> extract( Map<MutableWeaponMapKey, Verdict> rpsJudgements )
    {
        // note: HashMap.values.iterator iterates over collection with no order
        Verdict[] verdictArray = rpsJudgements.values().toArray( new Verdict[ rpsJudgements.size() ] );
        List<Verdict> verdicts = new ArrayList<>( verdictArray.length );
        Collections.addAll( verdicts, verdictArray );
        Collections.sort( verdicts );
        return verdicts;
    }
}
