/*
 * (c) sanecoders.com
 */
package com.sanecoders.immutablelife;

/**
 * @author Damian Czernous
 */
public enum Weapon
{
    ROCK, PAPER, SCISSORS;
}
