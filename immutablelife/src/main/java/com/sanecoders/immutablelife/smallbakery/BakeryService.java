/*
 * (c) sanecoders.com
 */

package com.sanecoders.immutablelife.smallbakery;

import com.sanecoders.immutablelife.smallbakery.dao.BakeryDao;
import com.sanecoders.immutablelife.smallbakery.dao.BakeryJpaDao;
import com.sanecoders.immutablelife.smallbakery.model.Bakery;

import java.util.List;

/**
 * @author Damian Czernous
 */
public class BakeryService
{
    private final BakeryDao bakeryDao = new BakeryJpaDao();

    public List<Bakery> findAll()
    {
        return bakeryDao.selectAll();
    }
}
