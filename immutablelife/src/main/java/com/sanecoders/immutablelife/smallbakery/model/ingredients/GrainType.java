/*
 * (c) sanecoders.com
 */

package com.sanecoders.immutablelife.smallbakery.model.ingredients;

/**
 * @author Damian Czernous
 */
public enum GrainType
{
    WHEAT, RYE
}
