/*
 * (c) sanecoders.com
 */

package com.sanecoders.immutablelife.smallbakery.dao;

import com.sanecoders.immutablelife.smallbakery.model.Bakery;
import com.sanecoders.immutablelife.smallbakery.model.Bread;
import com.sanecoders.immutablelife.smallbakery.model.Bun;
import com.sanecoders.immutablelife.smallbakery.model.FlavoredBread;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Damian Czernous
 */
public class BakeryLocalDao implements BakeryDao
{
    @Override
    public List<Bakery> selectAll()
    {
        String name = SampleData.getBakeryName();
        List<Bread> breads = SampleData.getBreads();
        List<FlavoredBread> flavoredBreads = SampleData.getFlavoredBreads();
        List<Bun> buns = SampleData.getBuns();

        List<Bakery> bakeries = new ArrayList<>( 1 );

        bakeries.add( new Bakery( name, breads, flavoredBreads, buns ) );

        return bakeries;
    }
}
