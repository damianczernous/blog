/*
 * (c) sanecoders.com
 */

package com.sanecoders.immutablelife.smallbakery.model;

import com.sanecoders.immutablelife.smallbakery.model.ingredients.GrainType;
import com.sanecoders.immutablelife.smallbakery.model.ingredients.Spice;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * @author Damian Czernous
 */
@Entity
@DiscriminatorValue("FlavoredBread")
public class FlavoredBread extends Bread
{
    Spice spice;

    /**
     * Constructor for JPA usage only. Brings no value to immutable object.
     */
    public FlavoredBread()
    {
        super();
    }

    public FlavoredBread( double taxRate, String name, GrainType grainType, Spice spice )
    {
        super( taxRate, name, grainType );
        this.spice = spice;
    }

    public final Spice getSpice()
    {
        return spice;
    }

    @Override
    public int hashCode()
    {
        return HashCodeBuilder.reflectionHashCode( this );
    }

    @Override
    public boolean equals( Object obj )
    {
        return EqualsBuilder.reflectionEquals( this, obj );
    }
}
