/*
 * (c) sanecoders.com
 */

package com.sanecoders.immutablelife.smallbakery;

import com.sanecoders.immutablelife.smallbakery.model.Bakery;
import com.sanecoders.immutablelife.smallbakery.model.Bread;
import com.sanecoders.immutablelife.smallbakery.model.Bun;
import com.sanecoders.immutablelife.smallbakery.model.FlavoredBread;
import com.sanecoders.immutablelife.smallbakery.view.BakeryView;

import java.util.List;

/**
 * @author Damian Czernous
 */
public class BakeryPresenter
{
    private final BakeryView bakeryView;
    private final BakeryService bakeryService;

    public BakeryPresenter( BakeryView bakeryView, BakeryService bakeryService )
    {
        this.bakeryView = bakeryView;
        this.bakeryService = bakeryService;
    }

    public void show()
    {
        List<Bakery> bakeries = bakeryService.findAll();

        fillViewWithBakeries( bakeries );

        if( !bakeries.isEmpty() )
        {
            bakeryView.print();
        }
    }

    private void fillViewWithBakeries( List<Bakery> bakeries )
    {
        for( Bakery bakery : bakeries )
        {
            bakeryView.setBakery( bakery.getName() );

            fillViewWithBreads( bakery.getBreads() );
            fillViewWithFlavoredBreads( bakery.getFlavoredBreads() );
            fillViewWithBuns( bakery.getBuns() );
        }
    }

    private void fillViewWithBreads( List<Bread> breads )
    {
        for( Bread bread : breads )
        {
            String grain = convert( bread.getGrainType() );
            String taxRate = convertToPercentageNotation( bread.getTaxRate() );

            bakeryView.addBread( bread.getName(), grain, null, taxRate );
        }
    }

    private void fillViewWithFlavoredBreads( List<FlavoredBread> flavoredBreads )
    {
        for( FlavoredBread flavoredBread : flavoredBreads )
        {
            String grain = convert( flavoredBread.getGrainType() );
            String spice = convert( flavoredBread.getSpice() );
            String taxRate = convertToPercentageNotation( flavoredBread.getTaxRate() );

            bakeryView.addBread( flavoredBread.getName(), grain, spice, taxRate );
        }
    }

    private void fillViewWithBuns( List<Bun> buns )
    {
        for( Bun bun : buns )
        {
            String filling = convert( bun.getFilling() );
            String topping = convert( bun.getTopping() );
            String taxRate = convertToPercentageNotation( bun.getTaxRate() );

            bakeryView.addBun( bun.getName(), filling, topping, taxRate );
        }
    }

    private static String convertToPercentageNotation( double taxRate )
    {
        return String.format( "%.0f %%", taxRate * 100 );
    }

    /**
     * @return enum value as string or empty string if null
     */
    private static String convert( Enum<?> e )
    {
        return ( e == null ) ? "" : e.toString();
    }
}
