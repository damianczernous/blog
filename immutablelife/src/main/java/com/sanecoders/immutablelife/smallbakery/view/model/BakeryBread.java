/*
 * (c) sanecoders.com
 */

package com.sanecoders.immutablelife.smallbakery.view.model;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

/**
 * @author Damian Czernous
 */
public final class BakeryBread implements Comparable<BakeryBread>
{
    private final String name;
    private final String grainType;
    private final String spice;
    private final String taxRate;

    public static BakeryBread create( String name, String grainType, String spice, String taxRate )
    {
        return new BakeryBread( parse( name ), parse( grainType ), parse( spice ), parse( taxRate ) );
    }

    private BakeryBread( String name, String grainType, String spice, String taxRate )
    {
        this.name = name;
        this.grainType = grainType;
        this.spice = spice;
        this.taxRate = taxRate;
    }

    public String getName()
    {
        return name;
    }

    public String getGrainType()
    {
        return grainType;
    }

    public String getSpice()
    {
        return spice;
    }

    public String getTaxRate()
    {
        return taxRate;
    }

    @Override
    public int compareTo( BakeryBread bakeryBread )
    {
        if( name == null || bakeryBread.getName() == null )
        {
            return 0;
        }

        return name.compareToIgnoreCase( bakeryBread.getName() );
    }

    @Override
    public int hashCode()
    {
        return HashCodeBuilder.reflectionHashCode( this );
    }

    @Override
    public boolean equals( Object obj )
    {
        return EqualsBuilder.reflectionEquals( this, obj );
    }

    private static String parse( String text )
    {
        return ( text != null ) ? text : "";
    }
}
