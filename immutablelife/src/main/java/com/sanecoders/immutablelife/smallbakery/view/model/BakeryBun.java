/*
 * (c) sanecoders.com
 */

package com.sanecoders.immutablelife.smallbakery.view.model;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

/**
 * @author Damian Czernous
 */
public final class BakeryBun implements Comparable<BakeryBun>
{
    private final String name;
    private final String filling;
    private final String topping;
    private final String taxRate;

    public static BakeryBun create( String name, String filling, String topping, String taxRate )
    {
        return new BakeryBun( parse( name ), parse( filling ), parse( topping ), parse( taxRate ) );
    }

    private BakeryBun( String name, String filling, String topping, String taxRate )
    {
        this.name = name;
        this.filling = filling;
        this.topping = topping;
        this.taxRate = taxRate;
    }

    public String getName()
    {
        return name;
    }

    public String getFilling()
    {
        return filling;
    }

    public String getTopping()
    {
        return topping;
    }

    public String getTaxRate()
    {
        return taxRate;
    }

    @Override
    public int compareTo( BakeryBun bakeryBun )
    {
        if( name == null || bakeryBun.getName() == null )
        {
            return 0;
        }

        return name.compareToIgnoreCase( bakeryBun.getName() );
    }

    @Override
    public int hashCode()
    {
        return HashCodeBuilder.reflectionHashCode( this );
    }

    @Override
    public boolean equals( Object obj )
    {
        return EqualsBuilder.reflectionEquals( this, obj );
    }

    private static String parse( String text )
    {
        return ( text != null ) ? text : "";
    }
}
