/*
 * (c) sanecoders.com
 */

package com.sanecoders.immutablelife.smallbakery.model;

import com.sanecoders.immutablelife.smallbakery.model.ingredients.Filling;
import com.sanecoders.immutablelife.smallbakery.model.ingredients.Topping;

/**
 * @author Damian Czernous
 */
public class MutableBun extends Bun
{
    public MutableBun()
    {
        this( 0.0d, null, null, null );
    }

    public MutableBun( double taxRate, String name, Filling filling, Topping topping )
    {
        super( taxRate, name, filling, topping );
    }

    @Override
    public void setTaxRate( double taxRate )
    {
        super.setTaxRate( taxRate );
    }

    @Override
    public void setName( String name )
    {
        super.setName( name );
    }

    @Override
    public void setFilling( Filling filling )
    {
        super.setFilling( filling );
    }

    @Override
    public void setTopping( Topping topping )
    {
        super.setTopping( topping );
    }
}
