/*
 * (c) sanecoders.com
 */

package com.sanecoders.immutablelife.smallbakery.model;

/**
 * @author Damian Czernous
 */
public enum PolishTaxRate
{
    /**
     * Bread, bread roll, etc. with shelf life under 14 days are subject to 5% tax.
     */
    SHELF_LIFE_14_DAYS( 0.05 ),

    /**
     * Doughnut, bun, etc. with shelf life under 14 days are subject to 8% tax.
     */
    SWEET_AND_SHELF_LIFE_14_DAYS( 0.08 ),

    /**
     * Any baked goods with shelf life above 14 days are subject to 23% tax.
     */
    SHELF_LIFE_ABOVE_14_DAYS( 0.23 );

    private final double taxRate;

    private PolishTaxRate( double taxRate )
    {
        this.taxRate = taxRate;
    }

    public final double get()
    {
        return taxRate;
    }
}
