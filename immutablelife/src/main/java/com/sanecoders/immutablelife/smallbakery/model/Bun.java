/*
 * (c) sanecoders.com
 */

package com.sanecoders.immutablelife.smallbakery.model;

import com.sanecoders.immutablelife.smallbakery.model.ingredients.Filling;
import com.sanecoders.immutablelife.smallbakery.model.ingredients.Topping;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * @author Damian Czernous
 */
@Entity
@DiscriminatorValue( "Bun" )
@Access( AccessType.PROPERTY )
public class Bun extends BaseEntity
{
    private double taxRate;
    private String name;
    private Filling filling;
    private Topping topping;

    /**
     * Constructor for JPA usage only. Brings no value to immutable object.
     */
    public Bun()
    {
        super();
    }

    public Bun( double taxRate, String name, Filling filling, Topping topping )
    {
        super();
        this.taxRate = taxRate;
        this.name = name;
        this.filling = filling;
        this.topping = topping;
    }

    void setTaxRate( double taxRate )
    {
        this.taxRate = taxRate;
    }

    void setName( String name )
    {
        this.name = name;
    }

    void setFilling( Filling filling )
    {
        this.filling = filling;
    }

    void setTopping( Topping topping )
    {
        this.topping = topping;
    }

    public final double getTaxRate()
    {
        return taxRate;
    }

    public final String getName()
    {
        return name;
    }

    public final Filling getFilling()
    {
        return filling;
    }

    public final Topping getTopping()
    {
        return topping;
    }

    @Override
    public int hashCode()
    {
        return HashCodeBuilder.reflectionHashCode( this );
    }

    @Override
    public boolean equals( Object obj )
    {
        return EqualsBuilder.reflectionEquals( this, obj );
    }
}
