/*
 * (c) sanecoders.com
 */

package com.sanecoders.immutablelife.smallbakery;

import com.sanecoders.immutablelife.smallbakery.view.BakeryView;

/**
 * @author Damian Czernous
 */
public class BakeryRunner
{
    public static void main( String[] args )
    {
        new BakeryPresenter( new BakeryView(), new BakeryService() ).show();
    }
}
