/*
 * (c) sanecoders.com
 */

package com.sanecoders.immutablelife.smallbakery.model;

import java.util.Collections;
import java.util.List;

/**
 * @author Damian Czernous
 */
public class MutableBakery extends Bakery
{
    public MutableBakery()
    {
        this( null,
              Collections.<Bread>emptyList(),
              Collections.<FlavoredBread>emptyList(),
              Collections.<Bun>emptyList() );
    }

    public MutableBakery( String name, List<Bread> breads, List<FlavoredBread> flavoredBreads, List<Bun> buns )
    {
        super( name, breads, flavoredBreads, buns );
    }

    public void setName( String name )
    {
        super.name = name;
    }

    public void setBreads( List<Bread> breads )
    {
        super.breads = breads;
    }

    public void setFlavoredBreads( List<FlavoredBread> flavoredBreads )
    {
        super.flavoredBreads = flavoredBreads;
    }

    public void setBuns( List<Bun> buns )
    {
        super.buns = buns;
    }
}
