/*
 * (c) sanecoders.com
 */

package com.sanecoders.immutablelife.smallbakery.model;

import com.sanecoders.immutablelife.smallbakery.model.ingredients.GrainType;
import com.sanecoders.immutablelife.smallbakery.model.ingredients.Spice;

/**
 * @author Damian Czernous
 */
public class MutableFlavoredBread extends FlavoredBread
{
    public MutableFlavoredBread()
    {
        this( 0.0d, null, null, null );
    }

    public MutableFlavoredBread( double taxRate, String name, GrainType grainType, Spice spice )
    {
        super( taxRate, name, grainType, spice );
    }

    public void setSpice( Spice spice )
    {
        super.spice = spice;
    }
}
