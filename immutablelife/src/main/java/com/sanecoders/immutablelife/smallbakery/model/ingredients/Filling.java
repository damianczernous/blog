/*
 * (c) sanecoders.com
 */

package com.sanecoders.immutablelife.smallbakery.model.ingredients;

/**
 * @author Damian Czernous
 */
public enum Filling
{
    CURD, JAM, PUDDING, APPLE, BLUEBERRIE
}
