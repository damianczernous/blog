/*
 * (c) sanecoders.com
 */

package com.sanecoders.immutablelife.smallbakery.model;

import javax.persistence.*;

/**
 * @author Damian Czernous
 */
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@DiscriminatorColumn(name = "Discriminator")
@DiscriminatorValue("BaseEntity")
public class BaseEntity
{
    /**
     * An ID generated with ORM. The hashcode and equals shouldn't relay on its value.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private long id;

    public final long getId()
    {
        return id;
    }

    @Override
    public int hashCode()
    {
        return 31;
    }

    @Override
    public boolean equals( Object obj )
    {
        if( this == obj ) return true;
        if( obj == null ) return false;
        if( getClass() != obj.getClass() ) return false;
        return true;
    }
}
