/*
 * (c) sanecoders.com
 */

package com.sanecoders.immutablelife.smallbakery.model;

import com.sanecoders.immutablelife.smallbakery.model.ingredients.GrainType;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * @author Damian Czernous
 */
@Entity
@DiscriminatorValue("Bread")
public class Bread extends BaseEntity
{
    double taxRate;
    String name;
    GrainType grainType;

    /**
     * Constructor for JPA usage only. Brings no value to immutable object.
     */
    public Bread()
    {
        super();
    }

    public Bread( double taxRate, String name, GrainType grainType )
    {
        super();
        this.taxRate = taxRate;
        this.name = name;
        this.grainType = grainType;
    }

    public final double getTaxRate()
    {
        return taxRate;
    }

    public final String getName()
    {
        return name;
    }

    public final GrainType getGrainType()
    {
        return grainType;
    }

    @Override
    public int hashCode()
    {
        return HashCodeBuilder.reflectionHashCode( this );
    }

    @Override
    public boolean equals( Object obj )
    {
        return EqualsBuilder.reflectionEquals( this, obj );
    }
}
