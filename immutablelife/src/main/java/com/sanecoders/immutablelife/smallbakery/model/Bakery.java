/*
 * (c) sanecoders.com
 */

package com.sanecoders.immutablelife.smallbakery.model;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author Damian Czernous
 */
@Entity
@DiscriminatorValue( "Bakery" )
@NamedQueries( { @NamedQuery( name = "Bakery.getBakeries", query = "SELECT b FROM Bakery b" ) } )
public class Bakery extends BaseEntity
{
    String name;

    @ElementCollection
    List<Bread> breads;

    @ElementCollection
    List<FlavoredBread> flavoredBreads;

    @ElementCollection
    List<Bun> buns;

    /**
     * Constructor for JPA usage only. Brings no value to immutable object.
     */
    public Bakery()
    {
        this( null,
              Collections.<Bread>emptyList(),
              Collections.<FlavoredBread>emptyList(),
              Collections.<Bun>emptyList() );
    }

    public Bakery( String name, List<Bread> breads, List<FlavoredBread> flavoredBreads, List<Bun> buns )
    {
        super();
        this.name = name;
        this.breads = new ArrayList<>( breads );
        this.flavoredBreads = new ArrayList<>( flavoredBreads );
        this.buns = new ArrayList<>( buns );
    }

    public final String getName()
    {
        return name;
    }

    public final List<FlavoredBread> getFlavoredBreads()
    {
        return Collections.unmodifiableList( flavoredBreads );
    }

    public final List<Bread> getBreads()
    {
        return Collections.unmodifiableList( breads );
    }

    public final List<Bun> getBuns()
    {
        return Collections.unmodifiableList( buns );
    }

    @Override
    public int hashCode()
    {
        return HashCodeBuilder.reflectionHashCode( this );
    }

    @Override
    public boolean equals( Object obj )
    {
        return EqualsBuilder.reflectionEquals( this, obj );
    }
}
