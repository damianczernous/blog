/*
 * (c) sanecoders.com
 */

package com.sanecoders.immutablelife.smallbakery.view;

import com.sanecoders.immutablelife.smallbakery.view.model.BakeryBread;
import com.sanecoders.immutablelife.smallbakery.view.model.BakeryBun;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author Damian Czernous
 */
public class BakeryView
{
    /**
     * Default column length; 1 unit = length of 1 char.
     */
    private final int COLUMN_LENGTH = 20;

    private static final String BAKERY_NAME = "Bakery: ";
    private static final String BAKERY_BREAD_LIST_TITLE = "Bakery breads.";
    private static final String BREAD_NAME_COLUMN = "Name";
    private static final String BREAD_GRAIN_TYPE_COLUMN = "Grain";
    private static final String BREAD_SPICE_COLUMN = "Spice";
    private static final String BREAD_TAX_COLUMN = "Tax";
    private static final String BAKERY_BUN_LIST_TITLE = "Bakery buns.";
    private static final String BUN_NAME_COLUMN = "Name";
    private static final String BUN_FILLING_COLUMN = "Filling";
    private static final String BUN_TOPPING_COLUMN = "Topping";
    private static final String BUN_TAX_COLUMN = "Tax";

    private final PrintStream userOut = System.out;

    private String bakeryName;
    private final List<BakeryBread> breads = new ArrayList<>();
    private final List<BakeryBun> buns = new ArrayList<>();

    public void print()
    {
        Collections.sort( breads );
        Collections.sort( buns );

        StringBuilder toPrint = new StringBuilder();

        toPrint.append( BAKERY_NAME + bakeryName ).append( "\n" ).append( "\n" );

        appendBreads( toPrint );
        appendBuns( toPrint );

        userOut.print( toPrint );
    }

    public void setBakery( String name )
    {
        this.bakeryName = ( name != null ) ? name : "";
    }

    public void addBread( String name, String grainType, String spice, String taxRange )
    {
        breads.add( BakeryBread.create( name, grainType, spice, taxRange ) );
    }

    public void addBun( String name, String filling, String topping, String taxRate )
    {
        buns.add( BakeryBun.create( name, filling, topping, taxRate ) );
    }

    private void appendBreads( StringBuilder out )
    {
        out.append( BAKERY_BREAD_LIST_TITLE ).append( "\n" );

        appendBreadHeader( out );

        for( BakeryBread bread : breads )
        {
            appendBread( out, bread );
        }

        out.append( "\n" );
    }

    private void appendBreadHeader( StringBuilder out )
    {
        appendCell( out, BREAD_NAME_COLUMN );
        appendCell( out, BREAD_GRAIN_TYPE_COLUMN );
        appendCell( out, BREAD_SPICE_COLUMN );
        appendCell( out, BREAD_TAX_COLUMN );
        out.append( "\n" );
    }

    private void appendBread( StringBuilder out, BakeryBread bread )
    {
        appendCell( out, bread.getName() );
        appendCell( out, bread.getGrainType() );
        appendCell( out, bread.getSpice() );
        appendCell( out, bread.getTaxRate() );
        out.append( "\n" );
    }

    private void appendBuns( StringBuilder out )
    {
        out.append( BAKERY_BUN_LIST_TITLE ).append( "\n" );

        appendBunHeader( out );

        for( BakeryBun bun : buns )
        {
            appendBun( out, bun );
        }

        out.append( "\n" );
    }

    private void appendBunHeader( StringBuilder out )
    {
        appendCell( out, BUN_NAME_COLUMN );
        appendCell( out, BUN_FILLING_COLUMN );
        appendCell( out, BUN_TOPPING_COLUMN );
        appendCell( out, BUN_TAX_COLUMN );
        out.append( "\n" );
    }

    private void appendBun( StringBuilder out, BakeryBun bun )
    {
        appendCell( out, bun.getName() );
        appendCell( out, bun.getFilling() );
        appendCell( out, bun.getTopping() );
        appendCell( out, bun.getTaxRate() );
        out.append( "\n" );
    }

    private void appendCell( StringBuilder out, String content )
    {
        out.append( content );

        for( int i = COLUMN_LENGTH - content.length(); i >= 0; i-- )
        {
            out.append( " " );
        }
    }
}
