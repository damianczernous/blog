/*
 * (c) sanecoders.com
 */

package com.sanecoders.immutablelife.smallbakery.dao;

import com.sanecoders.immutablelife.smallbakery.model.Bakery;

import java.util.List;

/**
 * @author Damian Czernous
 */
public interface BakeryDao
{
    List<Bakery> selectAll();
}
