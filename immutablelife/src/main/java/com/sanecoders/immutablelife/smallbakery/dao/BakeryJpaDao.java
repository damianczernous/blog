/*
 * (c) sanecoders.com
 */

package com.sanecoders.immutablelife.smallbakery.dao;

import com.sanecoders.immutablelife.smallbakery.model.Bakery;
import com.sanecoders.immutablelife.smallbakery.model.Bread;
import com.sanecoders.immutablelife.smallbakery.model.Bun;
import com.sanecoders.immutablelife.smallbakery.model.FlavoredBread;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import java.util.List;

/**
 * @author Damian Czernous
 */
public class BakeryJpaDao implements BakeryDao
{
    private final String BAKERY_PERSISTENCE_UNIT = "BakeryPU";
    private final EntityManager entityManager;

    {
        entityManager = Persistence.createEntityManagerFactory( BAKERY_PERSISTENCE_UNIT ).createEntityManager();
    }

    /*
     * Fills database with sample data.
     */
    {
        String name = SampleData.getBakeryName();
        List<Bread> breads = SampleData.getBreads();
        List<FlavoredBread> flavoredBreads = SampleData.getFlavoredBreads();
        List<Bun> buns = SampleData.getBuns();

        Bakery bakery = new Bakery( name, breads, flavoredBreads, buns );

        entityManager.getTransaction().begin();

        persist( breads );
        persist( flavoredBreads );
        persist( buns );

        entityManager.persist( bakery );
        entityManager.getTransaction().commit();
        entityManager.clear();
    }

    @Override
    public List<Bakery> selectAll()
    {
        entityManager.clear();

        return entityManager.createNamedQuery( "Bakery.getBakeries", Bakery.class ).getResultList();
    }

    private void persist( List<?> list )
    {
        for( Object item : list )
        {
            entityManager.persist( item );
        }
    }
}
