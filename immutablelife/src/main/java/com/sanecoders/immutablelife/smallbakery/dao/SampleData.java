/*
 * (c) sanecoders.com
 */

package com.sanecoders.immutablelife.smallbakery.dao;

import com.sanecoders.immutablelife.smallbakery.model.Bread;
import com.sanecoders.immutablelife.smallbakery.model.Bun;
import com.sanecoders.immutablelife.smallbakery.model.FlavoredBread;
import com.sanecoders.immutablelife.smallbakery.model.PolishTaxRate;
import com.sanecoders.immutablelife.smallbakery.model.ingredients.Filling;
import com.sanecoders.immutablelife.smallbakery.model.ingredients.GrainType;
import com.sanecoders.immutablelife.smallbakery.model.ingredients.Spice;
import com.sanecoders.immutablelife.smallbakery.model.ingredients.Topping;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Damian Czernous
 */
public class SampleData
{
    private static final String BAKERY_NAME = "Bakery on Bema square";
    private static final List<Bread> BREADS = new ArrayList<>();
    private static final List<FlavoredBread> FLAVORED_BREADS = new ArrayList<>();
    private static final List<Bun> BUNS = new ArrayList<>();

    static
    {

        BREADS.add( new Bread( PolishTaxRate.SHELF_LIFE_14_DAYS.get(), "Mazowiecki", GrainType.WHEAT ) );
        BREADS.add( new Bread( PolishTaxRate.SHELF_LIFE_14_DAYS.get(), "Knight", GrainType.WHEAT ) );
        BREADS.add( new Bread( PolishTaxRate.SHELF_LIFE_14_DAYS.get(), "Rye", GrainType.RYE ) );
        BREADS.add( new Bread( PolishTaxRate.SHELF_LIFE_14_DAYS.get(), "Poznan roll", GrainType.WHEAT ) );
        BREADS.add( new Bread( PolishTaxRate.SHELF_LIFE_14_DAYS.get(), "Wroclaw roll", GrainType.WHEAT ) );

        FLAVORED_BREADS.add( new FlavoredBread( PolishTaxRate.SHELF_LIFE_14_DAYS.get(),
                                                "Bread with cumin",
                                                GrainType.WHEAT,
                                                Spice.CUMIN ) );
        FLAVORED_BREADS.add( new FlavoredBread( PolishTaxRate.SHELF_LIFE_14_DAYS.get(),
                                                "Bread of onion",
                                                GrainType.WHEAT,
                                                Spice.ONION ) );
        FLAVORED_BREADS.add( new FlavoredBread( PolishTaxRate.SHELF_LIFE_14_DAYS.get(),
                                                "Brine roll",
                                                GrainType.WHEAT,
                                                Spice.SALT ) );

        BUNS.add( new Bun( PolishTaxRate.SWEET_AND_SHELF_LIFE_14_DAYS.get(), "Bun with curd", Filling.CURD, null ) );
        BUNS.add( new Bun( PolishTaxRate.SWEET_AND_SHELF_LIFE_14_DAYS.get(),
                           "Bun with berry",
                           Filling.BLUEBERRIE,
                           Topping.FROSTING ) );
    }

    public static String getBakeryName()
    {
        return BAKERY_NAME;
    }

    public static List<Bread> getBreads()
    {
        return BREADS;
    }

    public static List<FlavoredBread> getFlavoredBreads()
    {
        return FLAVORED_BREADS;
    }

    public static List<Bun> getBuns()
    {
        return BUNS;
    }
}
