/*
 * (c) sanecoders.com
 */

package com.sanecoders.immutablelife.smallbakery.model;

import com.sanecoders.immutablelife.smallbakery.model.ingredients.GrainType;

/**
 * @author Damian Czernous
 */
public class MutableBread extends Bread
{
    public MutableBread()
    {
        this( 0.0d, null, null );
    }

    public MutableBread( double taxRate, String name, GrainType grainType )
    {
        super( taxRate, name, grainType );
    }

    public void setTaxRate( double taxRate )
    {
        super.taxRate = taxRate;
    }

    public void setName( String name )
    {
        super.name = name;
    }

    public void setGrainType( GrainType grainType )
    {
        super.grainType = grainType;
    }
}
