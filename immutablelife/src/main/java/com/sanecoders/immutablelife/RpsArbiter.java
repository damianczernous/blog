/*
 * (c) sanecoders.com
 */

package com.sanecoders.immutablelife;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Damian Czernous
 */
public class RpsArbiter
{
    private static final Map<WeaponMapKey, Verdict> EVALUATION_MAP = new HashMap<>();

    static
    {
        EVALUATION_MAP.put( new WeaponMapKey( Weapon.ROCK, Weapon.ROCK ), Verdict.TIE );
        EVALUATION_MAP.put( new WeaponMapKey( Weapon.ROCK, Weapon.PAPER ), Verdict.LOSS );
        EVALUATION_MAP.put( new WeaponMapKey( Weapon.ROCK, Weapon.SCISSORS ), Verdict.WIN );
    }

    /**
     * @return {@link Verdict} for toEvaluate {@link Weapon}.
     */
    public Verdict judge( Weapon toEvaluate, Weapon against )
    {
        return EVALUATION_MAP.get( new WeaponMapKey( toEvaluate, against ) );
    }
}
