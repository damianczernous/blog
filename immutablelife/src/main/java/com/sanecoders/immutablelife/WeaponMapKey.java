/*
 * (c) sanecoders.com
 */
package com.sanecoders.immutablelife;

/**
 * @author Damian Czernous
 */
public final class WeaponMapKey
{
    private final Weapon weapon1;
    private final Weapon weapon2;

    public WeaponMapKey( Weapon weapon1, Weapon weapon2 )
    {
        this.weapon1 = weapon1;
        this.weapon2 = weapon2;
    }

    public Weapon getWeapon1()
    {
        return weapon1;
    }

    public Weapon getWeapon2()
    {
        return weapon2;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((weapon1 == null) ? 0 : weapon1.hashCode());
        result = prime * result + ((weapon2 == null) ? 0 : weapon2.hashCode());
        return result;
    }

    @Override
    public boolean equals( Object obj )
    {
        if( this == obj )
        {
            return true;
        }
        if( obj == null )
        {
            return false;
        }
        if( getClass() != obj.getClass() )
        {
            return false;
        }
        WeaponMapKey other = (WeaponMapKey)obj;
        if( weapon1 != other.weapon1 )
        {
            return false;
        }
        if( weapon2 != other.weapon2 )
        {
            return false;
        }
        return true;
    }
}
