/*
 * (c) sanecoders.com
 */
package com.sanecoders.immutablelife;

/**
 * @author Damian Czernous
 */
public enum Verdict
{
    WIN, TIE, LOSS;
}
